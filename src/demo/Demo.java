package demo;


enum WeightUnits {
    KG,LBS
}

enum HeightUnits {
    CENTIMETERS,INCHES
}

class Person{
    private String name;
    private Height h;
    private Weight w;
    public double BMI;

    public Person(String name, Height h, Weight w) {
        this.name = name;
        this.h = h;
        this.w = w;
        calcualteBMI();
    }
    
    public Height getH() {
        return h;
    }
    
    public void setH(Height h) {
        this.h = h;
        calcualteBMI();
    }

    public Weight getW() {
        return w;
    }

    public void setW(Weight w) {
        this.w = w;
        calcualteBMI();
    }  
    
    private void calcualteBMI(){
        if(h.hu == HeightUnits.CENTIMETERS){
            double heightInMeters = h.value / 100;
            if(w.wu == WeightUnits.KG){
                BMI = w.value / (heightInMeters * heightInMeters); 
            }
            //weight units are POUNDS
            else{
                //POUNDS to KG
                double weightInKG = w.value * 0.453592;
                BMI = weightInKG / (heightInMeters * heightInMeters);
            }
        }
        //height units are inches
        else{
           double heightInMeters = h.value * 2.54/100;
           if(w.wu == WeightUnits.KG){
                BMI = w.value / (heightInMeters * heightInMeters); 
            }
            //weight units are POUNDS
            else{
                //POUNDS to KG
                double weightInKG = w.value * 0.453592;
                BMI = weightInKG / (heightInMeters * heightInMeters);
            }
        }
    }
}


class Height{
    double value;
    HeightUnits hu;

    public Height(double value, HeightUnits hu) {
        this.value = value;
        this.hu = hu;
    }  
}


class Weight{
    double value;
    WeightUnits wu;
    public Weight(double value, WeightUnits wu) {
        this.value = value;
        this.wu = wu;
    }  
}

public class Demo {

    public static void main(String[] args) {
        
        
        Person P1 = new Person("person1",new Height(172,HeightUnits.CENTIMETERS),new Weight(82,WeightUnits.KG));
        System.out.println(P1.BMI);

        Person P2 = new Person("person2",new Height(70,HeightUnits.INCHES),new Weight(152,WeightUnits.LBS));
        System.out.println(P2.BMI);
    }
}
